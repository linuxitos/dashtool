<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Folder extends Model
{
    use HasFactory;
    protected $table = 'folders';

    protected $primaryKey = 'id';

    public $timestamps = false;

    protected $fillable = [
        'id', 'status', 'fc', 'desc', 'oname', 'gname', 'type', 'ext', 'size', 'path', 'link', 'folder_id', 'user_id',
    ];

    public function parent()
    {
        return $this->belongsTo(Folder::class, 'folder_id', 'id');
    }

    public function author()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function folders()
    {
        return $this->hasMany(Folder::class, 'folder_id', 'id')->where('type', 'folder')->where('status', '>', 0);
    }

    public function files()
    {
        return $this->hasMany(Folder::class, 'folder_id', 'id')->where('type', '<>', 'folder')->where('status', '>', 0);
    }
}
