<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Folder;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Response;

class FileController extends Controller
{
    protected $model;
    protected $attributeNames;
    protected $errorMessages;

    public function __construct(Folder $model)
    {
        $this->attributeNames = [
            'desc' => 'descripción',
            'desc_file' => 'descripción',
            'link' => 'enlace',
            'link_file' => 'enlace',
            'desc_up' => 'descripción',
        ];
        $this->errorMessages = [
            'requir ed' => 'El campo :attribute es obligatorio.',
        ];

        $this->model = $model;
    }

    public function files(Request $request)
    {
        $data['title'] = 'Módulo';
        $data['tab'] = 'main';
        $data['url'] = Route::current()->getName();
        if ($data['url'] !== '') {
            $data['permiso'] = auth()->user()->isPermitUrl($data);
            if ($data['permiso']) {
                $data['title'] = $data['permiso']->module->desc;
                $data['tab'] = $data['permiso']->parentModule->nom;

                return view('files/'.$data['url'], $data);
            } else {
                redireccionar(route('dashboard'), 'Módulo no autorizado.', 'danger');
            }
        } else {
            redireccionar(route('dashboard'), 'Permiso no encontrado.', 'danger');
        }
    }

    public function loadFolders(Request $request)
    {
        $data['page'] = ($request->page) ? $request->page : 1;
        $data['order'] = ($request->order) ? $request->order : 'desc';
        $data['order_by'] = ($request->order_by) ? $request->order_by : 'id';
        $data['search'] = ($request->search) ? trim($request->search) : '';
        $data['per_page'] = ($request->limite) ? $request->limite : 10;
        $data['status'] = ($request->act >= 0) ? $request->act : 0;
        $data['folder'] = ($request->folder >= 0) ? $request->folder : 0;
        $data['user'] = auth()->user()->id;
        $data['mview'] = ($request->mview >= 0) ? $request->mview : 'list';
        $data['act_fc'] = $request->act_fc >= 0 ? $request->act_fc : 0;
        $data['dt_ini'] = ($request->dt_ini) ? $request->dt_ini : getFirstLastDate('first', 'month');
        $data['dt_fin'] = ($request->dt_fin) ? $request->dt_fin : getFirstLastDate('last', 'month');
        $data['adyacentes'] = 2;

        $data['url'] = 'files';
        
        $total = $this->search($data, 1);
        $results = $this->search($data, 0);

        $total_pages = ceil($total / $data['per_page']);
        $res['total'] = $total;

        $res['data'] = '';
        if ($total > 0) {
            /*if ($data['mview'] == 'list') {*/
            $res['data'] .= '<div class="table-responsive"><table id="table-files" class="table table-files table-hover">
                    <thead class="table-head">
                        <tr class="row-link">
                            <th class="text-center w-3">
                                <div class="form-check form-check-sm form-check-custom me-3">
                                    <input class="form-check-input chk-delete-all" type="checkbox" data-kt-check="true" data-kt-check-target="#table-files .form-check-input" value="1" />
                                </div>
                            </th>
                            <th data-field="desc" class="th-link text-left w-20">'.($data['order_by'] == 'desc' ? ($data['order'] == 'desc' ? '<i class="fad fa-sort-down"></i>' : '<i class="fad fa-sort-up"></i>') : '<i class="fal fa-file-alt"></i>').' Nombre</th>
                            <th class="w-2"></th>
                            <th class="w-2"></th>';

            
            $res['data'] .= '<th data-field="size" class="th-link w-5 text-center" title="Clic para ordenar">'.($data['order_by'] == 'size' ? ($data['order'] == 'desc' ? '<i class="fad fa-sort-down"></i>' : '<i class="fad fa-sort-up"></i>') : '<i class="fad fa-list-ul"></i>').' Tamaño</th>
                                    <th data-field="fc" class="th-link w-5 text-center" title="Clic para ordenar">'.($data['order_by'] == 'fc' ? ($data['order'] == 'desc' ? '<i class="fad fa-sort-down"></i>' : '<i class="fad fa-sort-up"></i>') : '<i class="fal fa-clock"></i>').' Modificado</th>
                                </tr>
                            </thead>
                        <tbody>';
            foreach ($results as $reg) {
                $res['data'] .= '<tr data-id="'.$reg->id.'" class="ui-droppable">';
                $res['data'] .= '<td class="text-center w-3">
                                        <div class="form-check form-check-sm form-check-custom mt-2">
                                            <input class="form-check-input chk-select-delete" type="checkbox" data-id="'.$reg->id.'" value="1" id="chk_'.$reg->id.'" name="chk_'.$reg->id.'">
                                            <label for="chk_'.$reg->id.'" class="form-check-label d-none"> '.$reg->id.'</label>
                                        </div>
                                    </td>';
                if ($reg->ext == 'a.folder') {
                    $res['data'] .= '<td class="filename slt-row open-folder" data-user="' . $reg->user_id . '" data-nuser="' . $reg->author->name . '" data-id="' . $reg->id . '" title="Abrir carpeta">';
                } else {
                    $res['data'] .= '<td data-id="'.$reg->id.'" title="Ver archivo">';
                }
                $res['data'] .= '<div class="w-100">
                                    '.iconoArchivo($reg).'
                                    '.getNameFile($reg);
                $res['data'] .= '</div></td>';

                $res['data'] .= '<td class="">';

                if ($reg->ext == 'a.folder') {
                    $res['data'] .= '<a class="float-end dropdown-toggle mt-1" href="#" id="drop-act-'.$reg->id.'" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="text-decoration: none;">
                                        <i class="bi bi-three-dots fs-3"></i>
                                    </a>
                        <div class="dropdown-menu dropdown-menu-end" aria-labelledby="drop-act-'.$reg->id.'">';

                    $res['data'] .= '<a class="dropdown-item btn-up-desc" href="#" data-bs-toggle="modal" data-bs-target="#mdl-up-desc" data-id="'.$reg->id.'" data-nom="'.htmlspecialchars($reg->desc, ENT_QUOTES, 'UTF-8').'">
                                    <i class="align-middle bi bi-pencil-square"></i> Editar
                                </a>';
                    $res['data'] .= '<a class="dropdown-item text-danger mdl-del-list" href="#" id="btn-del-file-'.$reg->id.'" data-id="'.$reg->id.'" data-bs-toggle="modal" data-bs-target="#mdl-del-list" title="Eliminar" data-desc="'.htmlspecialchars($reg->desc, ENT_QUOTES, 'UTF-8').'">
                                    <i class="align-middle bi bi-trash"></i> Eliminar
                                </a>';

                    $res['data'] .= '<div class="dropdown-divider pb-0 mb-0"></div>';
                    $res['data'] .= '<span class="dropdown-item disabled pt-0 mt-0 text-center"><small title="'.$reg->fc.'">'.nicetime($reg->fc).'</small></span>';
                    $res['data'] .= '</div>';
                } else {
                    $res['data'] .= '<a class="float-end dropdown-toggle" href="#" id="drop-act-file-'.$reg->id.'" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="text-decoration: none;">
                            <i class="bi bi-three-dots fs-3 align-middle"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-end" aria-labelledby="drop-act-file-'.$reg->id.'">';

                    $res['data'] .= '<a class="dropdown-item btn-up-desc" href="#" title="Editar" data-bs-toggle="modal" data-bs-target="#mdl-up-desc" data-id="'.$reg->id.'" data-nom="'.htmlspecialchars($reg->desc, ENT_QUOTES, 'UTF-8').'"><i class="bi bi-pencil-square"></i> Editar</a>';

                    if ($reg->ext !== 'link' && $reg->ext !== 'comment' && $reg->ext !== 'link') {
                        $res['data'] .= '<a class="dropdown-item btn-download" href="'.route('download', $reg->gname).'" target="_blank" title="Descargar" data-nomfile="'.$reg->gname.'" data-id="'.$reg->id.'" download id="btn-download-link-'.$reg->id.'">
                            <i class="bi bi-download"></i> Descargar
                        </a>';
                    }

                    $res['data'] .= '<a class="dropdown-item text-danger mdl-del-list" href="#" id="btn-del-file-'.$reg->id.'" data-id="'.$reg->id.'"  data-bs-toggle="modal" data-bs-target="#mdl-del-list" title="Eliminar" data-desc="'.htmlspecialchars($reg->desc, ENT_QUOTES, 'UTF-8').'">
                            <i class="align-middle bi bi-trash"></i> Eliminar
                        </a>';

                    $res['data'] .= '<div class="dropdown-divider pb-0 mb-0"></div>';
                    $res['data'] .= '<span class="dropdown-item disabled pt-0 mt-0 text-center"><small title="'.$reg->fc.'">'.nicetime($reg->fc).'</small></span>';
                    $res['data'] .= '</div>';
                }

                $res['data'] .= '</td>';

                $res['data'] .= '<td class="text-center">';
                if ($reg->status == 1) {
                    $res['data'] .= '<button class="btn btn-link float-start btn-pub-file pt-0 pb-0 mt-2" id="btn-pub-file-'.$reg->id.'" data-id="'.$reg->id.'" title="Cambiar visibilidad">
                            <i class="text-success fal fa-lock align-middle"></i>
                        </button>';
                } else {
                    $res['data'] .= '<button class="btn btn-link float-start btn-pub-file pt-0 pb-0 mt-2" id="btn-pub-file-'.$reg->id.'" data-id="'.$reg->id.'" title="Cambiar visibilidad">
                                <i class="text-danger fal fa-globe align-middle"></i>
                        </button>';
                }

                if ($reg->ext == 'a.folder') {
                    $res['data'] .= '<td class="text-center align-middle">
                                        <small>
                                            '.(count($reg->files) + count($reg->folders)).((count($reg->files) + count($reg->folders)) == 1 ? ' elemento, ' : ' elementos, ').formatSizeUnits(intval($reg->files->sum('size'))).'
                                        </small>
                                    </td>';
                } else {
                    $res['data'] .= '<td class="text-center align-middle"><small>'.formatSizeUnits(intval($reg->size)).'</small></td>';
                }

                $res['data'] .= '<td class="text-center align-middle"> <small title="'.fecha($reg->fc, 'd/m/y h:m').'">'.nicetime($reg->fc).'</small></td>';
                $res['data'] .= '</tr>';
            }
            $res['data'] .= '</tbody></table></div>';
            $res['data'] .= '<div class="border-top">'.paginate($data['page'], $total_pages, $data['adyacentes'], 'load').'</div>';
            /*} else {
                $res['data'] .= '<div class="row mb-4">';
                foreach ($results as $reg) {
                    $res['data'] .= '<div class="col-xl-2 col-lg-2 col-md-3 col-sm-6 col-6 mb-2">
                                        <div class="card h-100 p-1">
                                            <div class="fs-7 fw-bold text-gray-400">
                                                <button class="btn btn-icon btn-pub-file" id="btn-pub-file-" data-id="" title="Cambiar visibilidad">
                                                    <i class="text-success fas fa-lock"></i>
                                                </button>
                                                <a class="float-end dropdown-toggle mt-2" href="#" id="drop-act-" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="text-decoration: none;">
                                                    <i class="bi bi-three-dots fs-2"></i>
                                                </a>
                                                <div class="dropdown-menu dropdown-menu-end" aria-labelledby="drop-act-">
                                                    <a class="dropdown-item btn-up-folder" href="#" data-bs-toggle="modal" data-bs-target="#mdl-up-folder" data-id="" data-nom="" data-desc="" data-pos="" data-show="">
                                                        <i class="align-middle bi bi-pencil-square"></i> Editar
                                                    </a>
                                                    <a class="dropdown-item text-danger mdl-del-list" href="#" id="btn-del-file-" data-id="" data-desc="" data-type="" data-bs-toggle="modal" data-bs-target="#mdl-del-list" title="Eliminar">
                                                        <i class="align-middle bi bi-trash"></i> Eliminar
                                                    </a>
                                                    <div class="dropdown-divider pb-0 mb-0"></div>
                                                    <span class="dropdown-item disabled pt-0 mt-0 text-center">
                                                        <small title="">hace 4 días</small>
                                                    </span>
                                                </div>
                                                <div class="form-check form-check-sm form-check-custom float-end mt-2 mx-4">
                                                    <input class="form-check-input chk-select-delete" type="checkbox" data-id="" value="1" id="chk_" name="chk_">
                                                    <label for="chk_" class="form-check-label d-none"></label>
                                                </div>
                                            </div>
                                            <div class="card-body d-flex justify-content-center text-center flex-column p-0 m-0">
                                                <a href="../../demo1/dist/apps/file-manager/files.html" class="text-gray-700 text-hover-primary d-flex flex-column p-0 m-0">
                                                    <div class="symbol symbol-100px mb-0 p-0 m-0 mb-n3">
                                                        '.iconoArchivoGrid($reg->ext).'
                                                    </div>
                                                    <div class="fs-6 fw-bolder mb-2 mt-0 folder-title">'.$reg->desc.'</div>
                                                </a>
                                                <div class="fs-7 fw-bold text-gray-400">0 elementos, 0 bytes</div>
                                            </div>
                                        </div>
                                    </div>';
                }
                $res['data'] .= '<div class="border-top mt-3">'.paginate($data['page'], $total_pages, $data['adyacentes'], 'load').'</div>';
                $res['data'] .= '</div>';
            }*/
        } else {
            $res['data'] = '<div class="row mt-2"><div class="col-md-12 p-9"><div class="text-center mt-2"><h6 class="text-center text-gray"><i class="fal fa-folder-open icon-40x"></i> <br></h6><h6 class="mt-3 text-gray">Carpeta vacía</h6></div></div></div>';
        }

        return response()->json($res);
    }

    public function loadPath(Request $request)
    {
        $data['id'] = ($request->folder) ? $request->folder : 0;
        $res['path'] = '';
        if ($data['id'] == 0) {
            $res['path'] .= '<nav aria-label="breadcrumb">
                                <ol class="breadcrumb breadcrumb-chevron p-2 bg-body-tertiary rounded-3">
                                    <li class="breadcrumb-item active" aria-current="page">
                                        <i class="fal fa-folder-open mx-1"></i> Raíz
                                    </li>
                                </ol>
                            </nav>';
        } else {
            $query = DB::select("SELECT
                                    @a AS _id, (SELECT @a := folder_id FROM folders WHERE id = _id) AS folder_id, IFNULL((SELECT `desc` FROM folders WHERE id = _id), 'Raíz') AS `desc`
                                FROM
                                    (SELECT @a := ".$data['id'].") vars, folders h WHERE @a <> 0 and user_id= ".auth()->user()->id);
            foreach ($query as $key => $value) {
                $paths[] = ['folder_id' => $value->folder_id, 'desc' => $value->desc];
            }

            $res['path'] .= '<nav aria-label="breadcrumb">
                                <ol class="breadcrumb breadcrumb-chevron p-2 bg-body-tertiary rounded-3">';
            $i = 0;
            foreach (array_reverse($paths) as $path) {
                if (strlen($path['desc']) > 15) {
                    $nom = get_words($path['desc'], 2).'...';
                } else {
                    $nom = $path['desc'];
                }
                $res['path'] .= '<li class="breadcrumb-item"><a href="#" class="open-folder link-body-emphasis fw-semibold text-decoration-none" data-id="' . $path['folder_id'] . '" title="Abrir: ' . $path['desc'] . '">'. ($nom=='Raíz'?'<i class="fal fa-home"></i> ':'<i class="fal fa-folder"></i> ') . $nom . '</a></li>';
            }
            $inf_fld = Folder::where('id', $data['id'])->first();
            $res['path'] .= '<li class="breadcrumb-item active" aria-current="page">
                                <i class="fal fa-folder-open mx-1"></i> <span class="text-muted" data-id="'.$inf_fld->id.'"> '.$inf_fld->desc.'</span>
							</li>';

            $res['path'] .= '</ol></nav>';
        }

        return response()->json($res);
    }

    public function addFolder(Request $request)
    {
        $data['gname'] = Str::random(5);
        $data['ufolder'] = 'user'.auth()->user()->id;
        $data['folder'] = $data['id'] = $request->folder ? trim($request->folder) : 0;
        if ($data['folder'] > 0) {
            $info_folder = Folder::where('id', $data['folder'])->first();
        }
        
        $rulesAdd = [
            'desc' => 'required|string|max:300',
        ];

        $validator = Validator::make($request->all(), $rulesAdd, $this->errorMessages)->setAttributeNames($this->attributeNames);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()]);
        } else {
            $addReg = [
                'desc' => $request->desc ? trim($request->desc) : '',
                'folder_id' => $data['folder'],
                'gname' => $data['gname'],
                'type' => 'folder',
                'ext' => 'a.folder',
                'path' => ($data['folder'] > 0 ? $info_folder->path.$data['gname'].'/' : $data['ufolder'].'/files/'.$data['gname'].'/'),
                'user_id' => auth()->user()->id,
            ];
            $exist = Folder::where('desc', $request->desc)->where('folder_id', $request->folder)->where('user_id', auth()->user()->id)->count();
            if ($exist == 0) {
                if (($fld = Folder::create($addReg))) {
                    $msg = ['tipo' => 'success',
                        'icon' => 'fa fa-check',
                        'msg' => 'Carpeta creada correctamente.', ];
                } else {
                    $msg = ['tipo' => 'danger',
                        'icon' => 'fa fa-times',
                        'msg' => 'Error interno, intenta más tarde.', ];
                }
            } else {
                $msg = ['tipo' => 'danger',
                    'icon' => 'fa fa-times',
                    'msg' => 'El nombre de la carpeta ya existe.', ];
            }
        }

        return response()->json($msg);
    }

    public function addFiles(Request $request)
    {
        $data['folder'] = $request->folder ? trim($request->folder) : 0;
        $data['type-upload'] = $request->type_upload ? trim($request->type_upload) : 'none';
        $data['gname'] = Str::random(5);

        $folder = Folder::find($data['folder']);
        
        if ($folder) {
            $data['path'] = $folder->path;
        }else{
            $data['path'] = 'user'.auth()->user()->id.'/files/';
        }

        if ($data['type-upload'] == 'file') {
            if (! is_dir(storage_path().'/'.$data['path'])) {
                mkdir(storage_path().'/'.$data['path'], 0775, true);
            }
            if ($request->hasfile('files')) {
                $filesAllowed = ['ppt', 'pdf', 'doc', 'docx', 'xls', 'xlsx', 'ods', 'odt', 'pptx', 'png', 'jpg', 'jpeg', 'gif', 'txt', 'mp3', 'ogg'];
                $cor = 0;
                $err = 0;
                $i = 1;
                foreach ($request->file('files') as $key => $file) {
                    if (in_array(strtolower($file->getClientOriginalExtension()), $filesAllowed)) {
                        $addFile = [
                            'desc' => trim($request->desc_file).(count($request->file('files'))>1?" ".$i:''),
                            'gname' => Str::random(5).'-'.Str::random(4).'.'.strtolower($file->getClientOriginalExtension()),
                            'type' => $file->getClientMimeType(),
                            'ext' => $file->getClientOriginalExtension(),
                            'size' => $file->getSize(),
                            'path' => $data['path'],
                            'folder_id' => $data['folder'],
                            'user_id' => auth()->user()->id,
                        ];
                        if ($file->move(storage_path().'/'.$data['path'], $addFile['gname']) && ($regAdd = Folder::create($addFile))) {
                            $cor++;
                        } else {
                            $err++;
                        }
                    } else {
                        $err++;
                    }
                    $i++;
                }
                $msg = ['tipo' => 'success',
                    'icon' => 'bi bi-check-circle',
                    'msg' => ($cor == 1 ? $cor.' archivo agregado' : ($cor > 1 ? $cor.' archivos agregados' : '')).($err > 0 ? '<br>'.($err == 1 ? $cor.' archivo no subidos' : ($cor > 1 ? $err.' archivos no subidos' : '')) : ''), ];
            } else {
                $msg = ['tipo' => 'danger',
                    'icon' => 'bi bi-x-circle',
                    'msg' => 'No archivos seleccionados', ];
            }
        }

        if ($data['type-upload'] == 'url') {
            $addReg = [
                'desc' => $request->desc_file ? trim($request->desc_file) : '',
                'link' => $request->link_file ? trim($request->link_file) : '',
                'folder_id' => $data['folder'],
                'gname' => $data['gname'],
                'type' => 'link',
                'ext' => 'link',
                'path' => $data['path'],
                'user_id' => auth()->user()->id,
            ];

            $rulesAdd = [
                'desc_file' => 'required|string|max:300',
                'link_file' => 'required|string|max:300',
            ];
            $validator = Validator::make($request->all(), $rulesAdd, $this->errorMessages)->setAttributeNames($this->attributeNames);
            if ($validator->fails()) {
                return response()->json(['errors' => $validator->errors()->all()]);
            } else {
                if (($regAdd = Folder::create($addReg))) {
                    $msg = ['tipo' => 'success',
                        'icon' => 'bi bi-check-circle',
                        'msg' => 'Archivo agregado', ];
                } else {
                    $msg = ['tipo' => 'danger',
                        'icon' => 'bi bi-x-circle',
                        'msg' => 'Error interno, intente más tarde', ];
                }
            }
        }

        return response()->json($msg);
    }

    public function download($filename)
    {
        $file = Folder::where('gname', $filename)->where('status', '>', 0)->first();
        if ($file) {
            $path = storage_path().'/'.$file->path.$filename;
            $type = $file->type;
        } else {
            $path = asset('public/app/images/404.png');
            $type = '';
        }
        $contents = file_get_contents($path);
        $response = Response::make($contents, 200);
        $response->header('Content-Type', $type);

        return $response;
    }

    public function delFiles(Request $request)
    {
        $list = $request->list ? $request->list : '';
        $act_file = $request->act_file >= 0 ? $request->act_file : 1;
        $regs = explode(',', $list);

        $acts[0] = 'eliminad';
        $acts[1] = 'retirad';
        $acts[2] = 'publicad';

        if (count($regs) > 0) {
            $files = 0;
            $folders = 0;
            foreach ($regs as $id) {
                $reg = Folder::find($id);
                if ($reg) {
                    if ($reg->ext == 'a.folder') {
                        $dir_file = $reg->path;
                        if (($total = Folder::where('path', 'LIKE', "{$dir_file}%")->where('status', '>=', 1)->update(['status' => $act_file]))) {
                            $folders++;
                        }
                    } else {
                        if (Folder::where('id', $id)->update(['status' => $act_file])) {
                            $files++;
                        }
                    }
                }
            }

            $msgFolders = ($folders > 0 ? ($folders == 1 ? $folders.' carpeta(más contenido) '.$acts[$act_file].($folders == 1 ? 'a;' : 'as;') : $folders.' carpetas(más contenido) '.$acts[$act_file].($folders == 1 ? 'a;' : 'as;')) : '');
            $msgFiles = ($files > 0 ? ($files == 1 ? ' '.$files.' archivo '.$acts[$act_file].($files == 1 ? 'o' : 'os') : ' '.$files.' archivos '.$acts[$act_file].($files == 1 ? 'o' : 'os')) : '');

            $msgTotal = $msgFolders.' '.$msgFiles;

            $msg = ['tipo' => 'success',
                'icon' => 'bi bi-check-circle',
                'msg' => ($folders == 0 && $files == 0 ? 'Sin cambios aplicados' : $msgTotal), ];
        } else {
            $msg = ['tipo' => 'danger',
                'icon' => 'bi bi-x-circle',
                'msg' => 'Selecciona al menos un registro', ];
        }

        return response()->json($msg);
    }

    public function pubFile(Request $request)
    {
        $data['id_file'] = $request->reg ? $request->reg : 0;
        $reg = Folder::find($data['id_file']);
        if ($reg) {
            if ($reg->ext == 'a.folder') {
                $dir_file = $reg->path;
                if ($reg->status == 1) {
                    if (($total = Folder::where('path', 'LIKE', "{$dir_file}%")->where('status', '>=', 1)->update(['status' => 2]))) {
                        $msg = ['tipo' => 'success',
                            'icon' => 'text-danger fal fa-globe',
                            'msg' => $total.($total == 1 ? ' carpeta' : ' carpetas').' y su contenido se publicaron. ', ];
                    } else {
                        $msg = ['tipo' => 'danger',
                            'icon' => 'text-danger fal fa-lock',
                            'msg' => 'Error interno, ningún cambio realizado.', ];
                    }
                } elseif ($reg->status == 2) {
                    if (($total = Folder::where('path', 'LIKE', "{$dir_file}%")->where('status', '>=', 1)->update(['status' => 1]))) {
                        $msg = ['tipo' => 'success',
                            'icon' => 'text-success fal fa-lock',
                            'msg' => $total.($total == 1 ? ' carpeta' : ' carpetas').' y su contenido se retiraron.', ];
                    } else {
                        $msg = ['tipo' => 'danger',
                            'icon' => 'text-danger fal fa-globe',
                            'msg' => 'Error interno, ningún cambio realizado.', ];
                    }
                }
            } else {
                if ($reg->status == 1) {
                    if (Folder::where('id', $request->reg)->update(['status' => 2])) {
                        $msg = ['tipo' => 'success',
                            'icon' => 'text-danger fal fa-globe',
                            'msg' => 'Carpeta y contenido se retiraron.', ];
                    } else {
                        $msg = ['tipo' => 'danger',
                            'icon' => 'text-success fal fa-lock',
                            'msg' => 'Error interno, ninguno cambio guardado', ];
                    }
                } elseif ($reg->status == 2) {
                    if (Folder::where('id', $request->reg)->update(['status' => 1])) {
                        $msg = ['tipo' => 'success',
                            'icon' => 'text-success fal fa-lock',
                            'msg' => 'Archivo retirado', ];
                    } else {
                        $msg = ['tipo' => 'danger',
                            'icon' => 'text-danger fal fa-globe',
                            'msg' => 'Error interno, ningún cambio guardado.', ];
                    }
                }
            }
        } else {
            $msg = ['tipo' => 'danger',
                'icon' => 'bi bi-x-circle',
                'msg' => 'No se encontró el registro, por favor reinicia sesión.', ];
        }

        return response()->json($msg);
    }

    public function upDesc(Request $request)
    {
        $info['id'] = $request->id ? trim($request->id) : 0;
        $info['desc'] = $request->desc_up ? trim($request->desc_up) : "";

        $file = Folder::find($info['id']);
        if ($file) {
            $rulesUp = [
                'desc_up' => 'required|string|max:300',
            ];
            $validator = Validator::make($request->all(), $rulesUp, $this->errorMessages)->setAttributeNames($this->attributeNames);
            if ($validator->fails()) {
                return response()->json(['errors' => $validator->errors()->all()]);
            } else {
                $exist = Folder::where('desc', $info['desc'])->where('folder_id', $file->folder_id)->where('id', '<>', $file->id)->where('ext', $file->ext)->count();
                if ($exist == 0) {
                    if (Folder::where('id', $request->id)->update(['desc'=>$info['desc']]) >= 0) {
                        $msg = ['tipo' => 'success',
                            'icon' => 'fa fa-check',
                            'msg' => 'Información actualizada.', ];
                    } else {
                        $msg = ['tipo' => 'danger',
                            'icon' => 'fa fa-times',
                            'msg' => 'Error interno, intenta más tarde.', ];
                    }
                } else {
                    $msg = ['tipo' => 'danger',
                        'icon' => 'fa fa-times',
                        'msg' => 'El nombre del archivo ya existe.', ];
                }
            }
        } else {
            $msg = ['tipo' => 'danger',
                'icon' => 'bi bi-x-circle',
                'msg' => 'No se encontró el registro, por favor reinicia sesión.', ];
        }

        return response()->json($msg);
    }

    private function search($data, $mode)
    {
        $query = $this->model;
        if ($data['act_fc'] == 1) {
            $query = $query->whereBetween('fc', [$data['dt_ini'], $data['dt_fin']]);
        }

        if ($data['status'] > 0) {
            $query = $query->where('status', $data['status']);
        } else {
            $query = $query->where('status', '>', 0);
        }

        $query = $query->where('user_id', $data['user']);

        if ($data['search'] != "" && $data['folder'] == 0) {
            $query = $query->where('folder_id', '>=', 0);
        } else {
            $query = $query->where('folder_id', $data['folder']);
        }
        
        $words = splitWordSearch($data['search']);
        if ($words) {
            $query = $query->where(function (Builder $q) use ($words) {
                foreach ($words as $word) {
                    $q->whereAny([
                        'desc',
                        'gname',
                    ], 'LIKE', '%'.$word.'%');
                }
            });
        }

        $query = $query->orderBy('ext', 'asc')->orderBy($data['order_by'], $data['order']);
        if ($mode == 0) {
            $data['offset'] = ($data['page'] - 1) * $data['per_page'];
            $query = $query->offset($data['offset'])->limit($data['per_page']);
            $query = $query->get();
        } else {
            $query = $query->count();
        }

        return $query;
    }
}
