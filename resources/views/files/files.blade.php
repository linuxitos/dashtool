@extends('layouts.appDash')
@section('breadcrumb')
	@include('layouts.partials._breadcrumbs')
@endsection

@section('content')
@include('files.mdls')

<div class="card mb-5 mb-xl-10">
    <div class="card-header border-0 d-none">
        <div class="card-title m-0">
            <h3 class="fw-bolder m-0">
                <i class="far fa-clipboard-list"></i> Posts
            </h3>
        </div>
    </div>
    <div class="card-body pt-4 pb-0">
        <form action="post" class="form-search" method="post" enctype="multipart/form-data" accept-charset="utf-8">
            <div class="row">
                <div class="col-lg-7 col-md-7 col-sm-7 col-7">
                    <div class="input-group">
                        <button id="btn-filter" class="btn btn-default dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                            <i class="fad fa-list"></i> <span class="d-none d-md-inline-block">Todos</span>
                        </button>
                        <ul class="dropdown-menu select-dropdown dropdown-edo">
                            <li>
                                <a class="dropdown-item" href="#" data-desc="Todos" data-icon="fad fa-list" data-edo="0" data-btn="btn-filter">
                                    <i class="fad fa-list"></i> Todos
                                </a>
                            </li>
                            <li>
                                <a class="dropdown-item" href="#" data-desc="Privado" data-icon="fal fa-lock" data-edo="1" data-btn="btn-filter">
                                    <i class="fal fa-lock"></i> Privado
                                </a>
                            </li>
                            <li>
                                <a class="dropdown-item" href="#" data-desc="Publicados" data-icon="fal fa-globe" data-edo="2" data-btn="btn-filter">
                                    <i class="fal fa-globe"></i> Publicados
                                </a>
                            </li>
                        </ul>

                        <button class="btn btn-default dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                            <i class="bi bi-funnel"></i>
                        </button>
                        <div class="no-cerrar dropdown-menu w-80">
                            <div class="form-row row g-3 p-2 m-0">
                                <div class="col-md-12 mb-2 mt-1">
                                    <div class="form-check form-switch">
                                        <input class="form-check-input" type="checkbox" name="chk-act-fc" id="chk-act-fc">
                                        <label class="form-check-label small" for="chk-act-fc">Utilizar fechas</label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <span class="input-group-text">Inicio</span>
                                        <input type="date" class="form-control date-range" name="dt-ini" id="dt-ini" value="{{todayMasD(0)}}" title="Fecha final" max="{{todayMasD(0)}}"/>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <span class="input-group-text">Fin</span>
                                        <input type="date" class="form-control" name="dt-fin" id="dt-fin" value="{{todayMasD(0)}}" title="Fecha final" max="{{todayMasD(0)}}"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <input id="txt-search" type="text" class="form-control pb-1 pt-1 border rounded-0 border-end-0 border-secondary" aria-label="Text" autocomplete="off">
                        <button type="submit" class="btn border border-secondary border-start-0 border-top border-end border-bottom btn-search">
                            <i class="bi bi-search"></i>
                        </button>
                    </div>
                </div>
                <div class="col-lg-5 col-md-5 col-sm-5 col-5">
                    <div class="btn-group float-end" role="group">
                        <button id="btn-del" type="button" class="btn btn-secondary btn-exist-data btn-del-files" data-bs-toggle="modal" data-bs-target="#mdl-del-list" disabled="disabled">
                            <i class="fas fa-check"></i>
                        </button>
                    </div>
    
                    <div class="btn-group float-end mx-1" role="group">
                        <button type="button" class="btn btn-secondary btn-reload" data-bs-toggle="tooltip" data-placement="bottom" title="" data-original-title="Refresh">
                            <i class="bi bi-arrow-clockwise"></i>
                        </button>
                    </div>
    
                    <div class="btn-group float-end" role="group">
                        <button type="button" class="btn btn-primary add-folder"  title='Agregar carpeta' data-bs-toggle="modal" data-bs-target="#mdl-add-folder">
                            <i class="bi bi-folder-plus"></i> <span class="d-none d-md-inline">Carpeta</span>
                        </button>
                        <button type="button" class="btn btn-success btn-add-files" id="btn-add-files" title='Agregar archivos' data-bs-toggle="modal" data-bs-target="#mdl-add-files">
                            <i class="fal fa-file-plus"></i> <span class="d-none d-md-inline">Archivos</span>
                        </button>
                    </div>
                </div>
            </div>
        </form>

        <div class="row mb-2 mt-2">
            <div id="div-path" class="col-md-10"></div>
            <div class="col-md-2">
                <div class="badge badge-lg badge-primary bg-primary float-end">
                    <span id="h5-cnt-total" class="float-end"></span>
                </div>
            </div>
        </div>
        <div class="row">
            <div id="div-cnt-load" class="col-md-12 mb-3"></div>
        </div>
    </div>
</div>

@endsection
@section('script')
    <script src="{{asset('public/vendor/lightbox/lightbox.js')}}"></script>
    <script src="{{asset('public/app/ajx/ajxfile.js')}}"></script>
    <script>
        url = "{{$permiso->url_sub}}";
        var url_string 	= window.location.href;
        var url_s 		= new URL(url_string);
        var fo 			= url_s.searchParams.get("folder");
        var us 			= url_s.searchParams.get("user");
        if (fo) {
            folder = fo;
        }else{
            folder = 0;
        }

        $(document).ready(function() {
            load(1);
            loadPath();
        });
    </script>
@endsection
