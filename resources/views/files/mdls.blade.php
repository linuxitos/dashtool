<div class="modal fade" id="mdl-add-folder" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" id="mdl-notify-add-folder">
		<div class="modal-content">
			<form id="form-add-folder" name="form-add-folder" accept-charset="utf-8" enctype="multipart/form-data" method="post">
				<div class="modal-header bg-gray-300 p-3">
					<h5 class="modal-title">
						<i class="bi bi-folder-plus"></i> Agregando carpeta
					</h5>
					<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12 text-center" id="div-cnt-msg-add-reg"></div>
						<div class="col-md-12 small mb-3" id="div-cnt-path-add"></div>
					</div>
					<input type="hidden" id="txt-folder-id" name="folder" readonly="" value="0">
					<div class="row mb-4">
					 	<div class="col-md-12">
							{!!inputText('desc', 'Nombre carpeta:', null, 'bi bi-folder', ['required' => 'required', 'placeholder' => ' ', 'maxlength'=>"100", 'autocomplete' => 'off'])!!}
						</div>
					 </div>
				</div>
				<div class="modal-footer p-2">
					<button type="button" class="btn btn-secondary" data-bs-dismiss="modal" id="btn-close-mdl-add-folder">
						<i class="bi bi-x-circle"></i> Cancelar
					</button>
					<button type="submit" class="btn btn-success" id="btn-add-folder">
						<i class="bi bi-check-circle"></i> Agregar
					</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="modal fade" id="mdl-add-files" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" id="mdl-notify-add-files">
		<div class="modal-content">
			<form id="form-add-files" method="post" accept-charset="utf-8" enctype="multipart/form-data">
				<div class="modal-header bg-gray-300 p-3">
					<h5 class="modal-title">
						<i class="fal fa-file-plus fs-3"></i> Agregando archivos
					</h5>
					<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
				</div>
				<div class="modal-body">
					<input type="hidden" id="txt-folder-id-add" name="folder" value="0" readonly="">
					<div class="row mt-2">
						<div class="col-md-4 position-relative">
							<div class="form-check form-check-inline start-50">
								<input class="form-check-input rad-type-up" data-type="file" type="radio" value="file" name="type_upload" id="type-file" checked="" required="">
								<label class="form-check-label" for="type-file">
									Subir archivo
								</label>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-check form-check-inline">
								<input class="form-check-input rad-type-up" data-type="url" type="radio" value="url" name="type_upload" id="type-link">
								<label class="form-check-label" for="type-link">
									Enlace externo
								</label>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<hr>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							{!!inputText('desc_file', 'Nombre:', null, 'bi bi-file-text', ['required' => 'required', 'placeholder' => ' ', 'maxlength'=>"200", 'autocomplete' => 'off'])!!}
						</div>
					</div>

					<div class="row">
						<div class="col-md-12 mt-3 up-file">
							<div class="mb-3">
								<label for="files-up" class="form-label">Seleccionar archivos</label>
								<input class="form-control req-file up-files" type="file" accept="image/jpeg,image/jpg,image/png, application/pdf, .xls, .doc, .docx, .xlsx, .ppt, .pptx, .mp3, .ogg, .txt" name="files[]" id="files-up" multiple="multiple" required="required">
							</div>
						</div>
						<!-- style="display: none;" -->
						<div class="col-md-12 up-file">
							<div class="progress">
								<div id="pro-bar-up-files" class="progress-bar bg-defult myprogress" role="progressbar" style="width: 105%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">0%</div>
							</div>
						</div>

						<div class="col-md-12 up-url mt-3" style="display: none;">
							{!!inputText('link_file', 'Enlace:', null, 'bi bi-link', ['url', 'placeholder' => ' ', 'maxlength'=>"400", 'autocomplete' => 'off'])!!}
						</div>
					</div>
				</div>
				<div class="modal-footer p-2">
					<button type="button" class="btn btn-secondary" data-bs-dismiss="modal" id="btn-close-mdl-add-file">
						<i class="bi bi-x-circle"></i> Cancelar
					</button>
					<button type="submit" class="btn btn-success" id="btn-add-file">
						<i class="bi bi-cloud-arrow-up"></i> Agregar
					</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="modal fade app-mdl" id="mdl-del-list" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content" id="mdl-notify-del-list">
			<div class="modal-header bg-gray-300 p-3">
				<h5 class="modal-title">
					<span class="bi bi-trash"></span> Eliminando archivos
				</h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<form method="post" id="form-del-list">
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12 text-center" id="div-msg-del-list"></div>
					</div>

                    {{-- <input type="hidden" id="txt-list-act" name="act_file" value="0" readonly=""> --}}
                    <div class="row">
						<div class="col-md-12 text-center" id="div-cnt-del-list"></div>
					</div>
                    <div class="row justify-content-center mt-3">
                        <div id="div-cnt-slt-act" class="col-md-6 mb-3">
							<div class="input-group">
								<span class="has-float-label">
									<select id="slt-act-file" class="form-select" name="act_file">
                                        <option value="" disabled="" selected="">-- Seleccionar --</option>
										<option value="1">Retirar</option>
										<option value="2">Publicar</option>
										<option value="0">Eliminar</option>
									</select>
									<label for="slt-act-file">Acción:</label>
								</span>
							</div>
						</div>
                    </div>
					<input type="hidden" id="txt-list-dfiles" name="list" value="" readonly="">
				</div>
				<div class="modal-footer p-2">
					<button type="button" class="btn btn-secondary" data-bs-dismiss="modal" id="btn-close-mdl-del-list">
						<i class="bi bi-x-circle"></i> Cerrar
					</button>
					<button type="submit" class="btn btn-success" disabled="disabled" id="btn-del-list">
						<i class="bi bi-check-circle"></i> Aceptar
					</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="modal fade" id="mdl-up-desc" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" id="mdl-ntf-up-desc">
		<div class="modal-content">
			<form class="form-up-desc" accept-charset="utf-8" enctype="multipart/form-data" method="post">
				<div class="modal-header bg-gray-300 p-3">
					<h5 class="modal-title">
						<i class="bi bi-pencil-square"></i> Actualizando descripción
					</h5>
					<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12 text-center" id="div-cnt-msg-up-desc"></div>
					</div>
					<input type="hidden" id="txt-up-file-id" name="id" readonly="" value="0">
					<div class="row mb-4">
					 	<div class="col-md-12">
							{!!inputText('desc_up', 'Nombre carpeta:', null, 'bi bi-folder', ['required' => 'required', 'placeholder' => ' ', 'maxlength'=>"100", 'autocomplete' => 'off'])!!}
						</div>
					 </div>
				</div>
				<div class="modal-footer p-2">
					<button type="button" class="btn btn-secondary" data-bs-dismiss="modal" id="btn-close-mdl-up-desc">
						<i class="bi bi-x-circle"></i> Cancelar
					</button>
					<button type="submit" class="btn btn-success" id="btn-up-desc">
						<i class="bi bi-check-circle"></i> Agregar
					</button>
				</div>
			</form>
		</div>
	</div>
</div>