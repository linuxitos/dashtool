var limite = 10;
var filter = 0;
var order = "desc";
var order_by = "id";
var search = "";
var reg = 0;
var url = "";
var user = 0;
var folder = 0;
var folderOld = 0;
var uslog = 0;
var mview = 'list';

var table;
var toolbarBase = document.querySelector('[data-kt-user-table-toolbar="base"]');
var toolbarSelected = document.querySelector('[data-kt-user-table-toolbar="selected"]');
var selectedCount = document.querySelector('[data-kt-user-table-select="selected_count"]');
const deleteSelected = document.querySelector('[data-kt-user-table-select="delete_selected"]');
// Detect checkboxes state & count
let checkedState = false;
let count = 0;
var list = [];

$('.dropdown-limit').find('a').click(function (e) {
    limite = $(this).data("edo");
    load(1);
    loadPath();
    e.preventDefault();
});

$('.dropdown-search').find('a').click(function (e) {
    lvlsearch = $(this).data("edo");
    if (lvlsearch == 1) {
        folder == 0;
    }
    load(1);
    loadPath();
    e.preventDefault();
});

$('.dropdown-edo').find('a').click(function (e) {
    filter = $(this).data("edo");
    load(1);
    loadPath();
    e.preventDefault();
});

$('.dropdown-users').find('a').click(function (e) {
    user = $(this).data("edo");
    folder = 0;
    load(1);
    loadPath();
    e.preventDefault();
});

$(document).on("submit", ".form-search", function (event) {
    var parametros = $(this).serialize();
    search = $('#txt-search').val();
    if (search == '') {
        folder = folderOld;
        lvlsearch = 0;
    } else {
        folder = 0;
        lvlsearch = 1;
    }
    load(1);
    loadPath();
    event.preventDefault();
});

function load(page = 1) {
    $.ajax({
        type: 'POST',
        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
        url: base_url + '/loadFolders',
        method: 'POST',
        dataType: 'JSON',
        data: {
            page: page,
            search: search,
            act: filter,
            limite: limite,
            order: order,
            order_by: order_by,
            user: user,
            folder: folder,
            mview: mview,
            act_fc: ($('#chk-act-fc').is(':checked') ? 1 : 0),
            dt_ini: $('#dt-ini').val(),
            dt_fin: $('#dt-fin').val(),
        },
        beforeSend: function (objeto) {
            $('.btn-search').html('<span class="spinner-border spin-x" role="status" aria-hidden="true"></span> Buscando...');
            $("#div-cnt-load").html('<div class="text-center alert alert-dark" role="alert">' +
                '<span class="spinner-border spin-x" role="status" aria-hidden="true"></span> Buscando...</div>');
        },
        success: function (res) {
            $('#div-cnt-load').html(res.data);
            $('#h5-cnt-total').html(res.total + (res.total == 1 ? ' elemento ' : ' elementos'));
            $('.btn-search').html('<i class="bi bi-search"></i>');
            checkedState = false;
            count = 0;
            list = [];
            $('.btn-exist-data').attr("disabled", true);
            $('.btn-exist-data').html('<i class="fas fa-check"></i>');
            //clearSelected();
            if (history.pushState) {
                var newurl = window.location.protocol + "//" + window.location.host + window.location.pathname + '?folder=' + folder + (search != "" ? '&search=' + search : '') + (user > 0 ? '&user=' + user : '');
                window.history.pushState({
                    path: newurl
                }, '', newurl);
            }
            //$('audio').initAudioPlayer();
            //notCreateFolder();
        },
        error: function (data) {
            $(".btn-search").html('<i class="bi bi-search"></i>');
            $("#div-cnt-load").html('<div class="text-center alert alert-danger" role="alert"><i class="fas fa-exclamation-circle"></i> Error interno, intenta más tarde.</div>');
        }
    });
}

$(document).on("click", ".table th.th-link", function () {
    if (order == "asc") {
        order = "desc";
    } else {
        order = "asc";
    }
    order_by = $(this).attr("data-field");
    load(1);
});

$(document).on("click", ".chk-select-delete", function () {
    var table = document.getElementById('table-files');
    allCheckboxes = table.querySelectorAll('tbody [type="checkbox"]');
    count = 0;
    checkedState = false;
    list = [];
    // Count checked boxes
    allCheckboxes.forEach(c => {
        if (c.checked) {
            checkedState = true;
            count++;
            list.push($(c).data('id'));
        }
    });
    // Toggle toolbars
    if (checkedState) {
        $('.btn-exist-data').attr("disabled", false);
        $('.btn-exist-data').html('<i class="fas fa-check"></i> ' + count + (count == 1 ? ' elemento' : ' elementos'));
    } else {
        $('.btn-exist-data').attr("disabled", true);
        $('.btn-exist-data').html('<i class="fas fa-check"></i>');
    }
});

$(document).on("click", ".chk-delete-all", function () {
    var table = document.getElementById('table-files');
    allCheckboxes = table.querySelectorAll('tbody [type="checkbox"]');
    count = 0;
    checkedState = false;
    list = [];
    if ($(this).prop("checked")) {
        allCheckboxes.forEach(c => {
            $(c).prop('checked', true);
        });
        checked = true;
    } else {
        allCheckboxes.forEach(c => {
            $(c).prop('checked', false);
        });
        checked = false;
    }

    allCheckboxes.forEach(c => {
        if (c.checked) {
            checkedState = checked;
            list.push($(c).data('id'));
            count++;
        }
    });

    // Toggle toolbars
    if (checkedState) {
        $('.btn-exist-data').attr("disabled", false);
        $('.btn-exist-data').html('<i class="fas fa-check"></i> ' + count + (count == 1 ? ' elemento' : ' elementos'));
    } else {
        $('.btn-exist-data').attr("disabled", true);
        $('.btn-exist-data').html('<i class="fas fa-check"></i>');
    }
});

$(document).on("click", ".btn-del-files", function () {
    $("#txt-list-dfiles").val(list);
    $('#slt-act-file').attr("required", true);
    $('#slt-act-file').val('').change();
    $("#div-cnt-slt-act").removeClass('d-none');
    var msg = '<span class="fs-4">Seleccionar acción para ' + (count == 1 ? '' : 'los') + '<span class="text-danger fw-bold fs-4"> ' + count + ' ' + (count == 1 ? 'registro seleccionado' : 'registros seleccionados') + '</span></span>';
    $("#div-cnt-del-list").html(msg);
    $('#btn-del-list').attr("disabled", false);
});

$(document).on("click", ".mdl-del-list", function () {
    var msg = '';
    $('#slt-act-file').val('0').change();
    $("#div-cnt-slt-act").addClass('d-none');
    $("#txt-list-dfiles").val($(this).data("id"));
    msg = '<span class="fs-4">¿Eliminar <span class="text-danger fw-bold fs-4"> ' + $(this).data("desc") + '</span>?</span>';
    $("#div-cnt-del-list").html(msg);
    $('#btn-del-list').attr("disabled", false);
});

$("#form-del-list").submit(function (event) {
    var parametros = $(this).serialize();
    $.ajax({
        type: "POST",
        dataType: "JSON",
        url: base_url + "/delFiles",
        data: parametros,
        beforeSend: function (datos) {
            $("#div-msg-del-list").html(
                '<div class="alert alert-info" role="alert">' +
                '<span class="spinner-border spin-x" role="status" aria-hidden="true"></span>' +
                ' Procesando</div>'
            );
            $('#btn-del-list').attr("disabled", true);
            $('#btn-del-list').html('<span class="spinner-border spin-x" role="status" aria-hidden="true"></span> Procesando');
        },
        success: function (datos) {
            if (datos.tipo == "success") {
                $("#btn-close-mdl-del-list").trigger("click");
                notifyMsg(datos.msg, '#', datos.tipo, '');
                load(1);
                $("#form-del-list")[0].reset();
                $("#div-msg-del-list").html('');
                $("#txt-list-dfiles").val(0);
            } else {
                $("#div-msg-del-list").html('<div class="alert alert-' + datos.tipo + ' alert-dismissible text-center" role="alert"><i class="' + datos.icon + '"></i>' +
                    datos.msg + '</div>');
            }
            setTimeout(function () {
                $("#div-msg-del-list").html('');
            }, 3000);
            $('#btn-del-list').attr("disabled", false);
            $('#btn-del-list').html('<i class="bi bi-check-circle"></i> Aceptar');
        },
        error: function (response) {
            $('#btn-del-list').attr("disabled", false);
            $('#btn-del-list').html('<i class="bi bi-check-circle"></i> Aceptar');
            $("#div-msg-del-list").html('<div class="alert alert-danger alert-dismissible text-center" role="alert"><i class="bi bi-exclamation-circle"></i>' +
                ' Error interno, intenta más tarde..</div>');
            setTimeout(function () {
                $("#div-msg-del-list").html('');
            }, 3000);
        }
    });
    event.preventDefault();
});

function loadPath() {
    $.ajax({
        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
        type: "POST",
        url: base_url + "/loadPath",
        dataType: "JSON",
        data: {
            folder: folder,
        },
        beforeSend: function (objeto) {
            $("#div-path").html('<span class="spin-hj spin-x" role="status" aria-hidden="true"></span> Cargando ...');
        },
        success: function (data) {
            $("#div-path").html(data.path);
        },
        error: function (datos) {
            $("#div-path").html('<div class="alert alert-danger" role="alert">' +
                '<i class="fas fa-exclamation-circle"></i> Error en la petición, intenta más tarde.</div>');
        }
    });
}

$(document).on("click", ".btn-reload", function (event) {
    search = $('#txt-search').val();
    load(1);
    event.preventDefault();
});

$(document).on("click", ".open-folder", function (e) {
    folder = $(this).data('id');
    var nuser = $(this).data('user');
    folderOld = folder;
    load(1);
    loadPath();
    e.stopPropagation();
});

$(document).on("click", ".open-locate", function (e) {
    folder = $(this).data("id");
    idfile = $(this).data("file");
    var nuser = $(this).data('user');
    if (nuser != user && nuser > 0) {
        user = nuser;
    }
    if (folder > 0) {
        $("#btn-add-files").attr("disabled", false);
    } else {
        $("#btn-add-files").attr("disabled", true);
    }
    lvlsearch = 0;
    folderOld = folder;
    load(1);
    loadPath();
    e.preventDefault();
});

$(document).on("click", ".add-folder", function () {
    $("#div-cnt-msg-add-reg").html("");
    $("#form-add-folder")[0].reset();
    $("#txt-folder-id").val(folder);
});

$("#form-add-folder").submit(function (event) {
    var parametros = $(this).serialize();
    $.ajax({
        type: "POST",
        url: base_url + "/addFolder",
        data: parametros,
        dataType: 'JSON',
        beforeSend: function (datos) {
            $("#btn-add-folder").html('<span class="spinner-border spinner-border-sm spin-x" role="status" aria-hidden="true"></span>');
            $('#btn-add-folder').attr("disabled", true);
        },
        success: function (datos) {
            if (datos.tipo == "success") {
                $("#btn-close-mdl-add-folder").trigger("click");
                $("#form-add-folder")[0].reset();
                notifyMsg(datos.msg, '#', datos.tipo, '');
                load(1);
            } else {
                if (datos.errors) {
                    jQuery.each(datos.errors, function (key, value) {
                        notifyMsg(value, '#', datos.tipo, 'mdl-notify-add-folder');
                    });
                } else {
                    notifyMsg(datos.msg, '#', datos.tipo, 'mdl-notify-add-folder');
                }
            }
            $("#btn-add-folder").html('<i class="bi bi-check-circle"></i> Aceptar');
            $('#btn-add-folder').attr("disabled", false);
        },
        error: function (response) {
            $("#btn-add-folder").html('<i class="bi bi-check-circle"></i> Aceptar');
            $('#btn-add-folder').attr("disabled", false);
            notifyMsg('Error interno, intenta más tarde', '#', 'danger', 'mdl-notify-add-folder');
        }
    });
    event.preventDefault();
});

$(document).on("click", ".rad-type-up", function () {
    var type = $(this).data("type");

    if (type == "file") {
        $(".up-file").show();
        $('.req-file').attr("required", true);
        $('.req-uname').attr("required", true);

        $(".up-url").hide();
        $('.req-url').attr("required", false);
    }

    if (type == "url") {
        $(".up-url").show();
        $('.req-uname').attr("required", true);
        $('.req-url').attr("required", true);

        $(".up-file").hide();
        $('.req-file').attr("required", false);
    }
});

$(document).on("click", ".btn-add-files", function () {
    $("#form-add-files")[0].reset();
    $("#txt-folder-id-add").val(folder);
});


$("#form-add-files").submit(function (event) {
    var typeUp = $(".rad-type-up:checked").val();

    if (typeUp == "file") {
        var parametros = new FormData(this);
        parametros.append('folder', folder);

        var fl = document.getElementById('files-up');
        var ln = fl.files.length;
        if (ln <= 0) {
            notifyMsg("Seleccione al menos un archivo.", '#', 'warning', 'mdl-notify-add-files');
        } else {
            if (ln > 5) {
                notifyMsg("Seleccione máximo 5 archivos.", '#', 'warning', 'mdl-notify-add-files');
            } else {
                $.ajax({
                    url: base_url + "/addFiles",
                    data: parametros,
                    type: 'POST',
                    contentType: false,
                    cache: false,
                    processData: false,
                    xhr: function () {
                        var xhr = new window.XMLHttpRequest();
                        xhr.upload.addEventListener("progress", function (evt) {
                            if (evt.lengthComputable) {
                                var percentComplete = evt.loaded / evt.total;
                                percentComplete = parseInt(percentComplete * 100);
                                $('#pro-bar-up-files').text(percentComplete + '%');
                                $('#pro-bar-up-files').css('width', percentComplete + '%');
                            }
                        }, false);
                        return xhr;
                    },
                    beforeSend: function (objeto) {
                        $("#pro-bar-up-files").removeAttr("class").attr("class", "bg-success text-center");
                        $('#pro-bar-up-files').css('width', '100');
                        $('#btn-add-file').attr("disabled", true);
                        $('#btn-add-file').html('<span class="spinner-border spin-x" role="status" aria-hidden="true"></span> Subiendo ...');
                    },
                    success: function (data) {
                        $('#pro-bar-up-files').css('width', 100 + '%');
                        $('#pro-bar-up-files').text('0%');
                        $("#div-cnt-imgs-all").html("");
                        if (data.tipo == 'success') {
                            setTimeout(function () {
                                $("#pro-bar-up-files").removeAttr("class").attr("class", "bg-default text-center");
                            }, 3000);
                            $("input[name=type_upload][value=" + typeUp + "]").prop('checked', true);
                            load(1);
                        } else {
                            $("#pro-bar-up-files").removeAttr("class").attr("class", "bg-danger text-center");
                            setTimeout(function () {
                                $("#pro-bar-up-files").removeAttr("class").attr("class", "bg-default text-center");
                            }, 3000);
                        }
                        $('#btn-add-file').attr("disabled", false);
                        $('#btn-add-file').html('<i class="bi bi-cloud-arrow-up"></i> Agregar');
                        $("#form-add-files")[0].reset();
                        notifyMsg(data.msg, '#', data.tipo, 'mdl-notify-add-files');
                    },
                    error: function (data) {
                        $('#btn-add-file').attr("disabled", false);
                        $('#btn-add-file').html('<i class="bi bi-cloud-arrow-up"></i> Agregar');
                        $('#pro-bar-up-files').css('width', 100 + '%');
                        $('#pro-bar-up-files').text('0%');
                        $("#pro-bar-up-files").removeAttr("class").attr("class", "bg-danger text-center");
                        setTimeout(function () {
                            $("#pro-bar-up-files").removeAttr("class").attr("class", "bg-default text-center");
                            $('#pro-bar-up-files').text('0%');
                        }, 3000);
                        notifyMsg("Error interno, intenta más tarde.", '#', 'danger', 'mdl-notify-add-files');
                    }
                });
            }
        }
    } else {
        var parametros = $(this).serialize();
        $.ajax({
            type: "POST",
            dataType: "JSON",
            url: base_url + "/addFiles",
            data: parametros,
            beforeSend: function (datos) {
                $('#btn-add-file').attr("disabled", true);
                $('#btn-add-file').html('<span class="spinner-border spin-x" role="status" aria-hidden="true"></span> Agregando');
            },
            success: function (datos) {
                if (datos.tipo == "success") {
                    $("#form-add-files")[0].reset();
                    $("input[name=type_upload][value=" + typeUp + "]").prop('checked', true);
                    load(1);
                }
                if (datos.errors) {
                    jQuery.each(datos.errors, function (key, value) {
                        notifyMsg(value, '#', 'danger', 'mdl-notify-add-files');
                    });
                } else {
                    notifyMsg(datos.msg, '#', datos.tipo, 'mdl-notify-add-files');
                }
                $('#btn-add-file').attr("disabled", false);
                $('#btn-add-file').html('<i class="bi bi-cloud-arrow-up"></i> Agregar');
            },
            error: function (response) {
                $('#btn-add-file').attr("disabled", false);
                $('#btn-add-file').html('<i class="bi bi-cloud-arrow-up"></i> Agregar');
                notifyMsg("Error interno, intenta más tarde.", '#', 'danger', 'mdl-notify-add-files');
            }
        });
    }
    event.preventDefault();
});

$(document).on("click", ".btn-up-desc", function () {
    $("#div-cnt-msg-up-desc").html("");
    $(".form-up-desc")[0].reset();
    $("#txt-up-file-id").val($(this).data("id"));
    $("#desc_up").val($(this).data("nom"));
    $("#txt-up-file-ext").val($(this).data("ext"));
});

$(document).on("click", ".btn-pub-file", function () {
    var idfile = $(this).data('id');
    var icon = $("#btn-pub-file-" + idfile).html();
    $.ajax({
        type: "POST",
        dataType: 'JSON',
        url: base_url + "/pubFile",
        data: {
            reg: idfile,
        },
        beforeSend: function (objeto) {
            $("#btn-pub-file-" + idfile).html('<span class="spinner-border spin-x" role="status" aria-hidden="true"></span>');
            $('#btn-pub-file-' + idfile).attr("disabled", true);
        },
        success: function (datos) {
            if (datos.tipo == "success") {
                $("#btn-pub-file-" + idfile).html('<i class="bi text-success bi-check-circle"></i>');
                setTimeout(function () {
                    $("#btn-pub-file-" + idfile).html('<i class="' + datos.icon + '"></i>');
                }, 2000);
            } else {
                $("#btn-pub-file-" + idfile).html(icon);
            }
            $('#btn-pub-file-' + idfile).attr("disabled", false);
            notifyMsg(datos.msg, '#', datos.tipo, '');
        },
        error: function (data) {
            $("#btn-pub-file-" + idfile).html(icon);
            $('#btn-pub-file-' + idfile).attr("disabled", false);
            notifyMsg("Error interno, intenta más tarde.", '#', 'danger', '');
        }
    });
});

$(document).on("submit", ".form-up-desc", function (e) {
    var parametros = $(this).serialize();
    $.ajax({
        type: "POST",
        dataType: "JSON",
        url: base_url + "/upDesc",
        data: parametros,
        beforeSend: function (datos) {
            $('#btn-up-desc').attr("disabled", true);
            $('#btn-up-desc').html('<span class="spinner-border spin-x" role="status" aria-hidden="true"></span> Actualizando');
        },
        success: function (datos) {
            if (datos.tipo == "success") {
                $("#btn-close-mdl-up-desc").trigger("click");
                $(".form-up-desc")[0].reset();
                notifyMsg(datos.msg, '#', datos.tipo, '');
                load(1);
            } else {
                if (datos.errors) {
                    jQuery.each(datos.errors, function (key, value) {
                        notifyMsg(value, '#', 'danger', 'mdl-notify-up-folder');
                    });
                } else {
                    notifyMsg(datos.msg, '#', datos.tipo, 'mdl-notify-up-folder');
                }
            }
            $('#btn-up-desc').attr("disabled", false);
            $('#btn-up-desc').html('<i class="bi bi-check-circle"></i> Actualizar');
        },
        error: function (response) {
            $('#btn-up-desc').attr("disabled", false);
            $('#btn-up-desc').html('<i class="bi bi-check-circle"></i> Actualizar');
            notifyMsg('Error interno, intenta más tarde', '#', 'danger', 'mdl-notify-up-folder');
        }
    });

    e.preventDefault();
});