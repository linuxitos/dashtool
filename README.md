# DashTool

## Descripción

Un dashboard utilizando Laravel 11 con algunas características esenciales desarrolladas a mi manera. Éste proyecto lo utilicé como parte de la facultad, por lo que sólo lo actualicé a la versión más reciente de Laravel, para compartirlo con ustedes.

## Funcionalidades

- Control de permisos
- Control de usuario
- Control de módulos
- Uso ajax
- PHP > 8.2
- Buscador con ajax
- Login
- Registro
- Recuperación de contraseña
- Uso de imágenes
- Gráficas
- Exportado de Excel
- Exportado de PDF
- Administrador de archivos
- Pre-visualizador de imágenes
- Uso de descarga en base 65 para encriptar la ruta del archivo

## Requerimientos

Éstos requerimientos son necesarios, y si nos los tienen instalados, es indispensable que se realice antes de continuar:

- Composer
- GIT
- XAMPP > 8.2 ó PHP nativo > 8.2
- Sistemas operativos compatibles con los requerimientos anteriores

# Implementación

## Clonar proyecto
- Clonar el proyecto en la ruta de xampp/htdocs/ si se usa laragon o algún otro servidor de PHP, ubicarlo en el directorio por defecto.

[Linux/Mac] usando la terminal normal en [Windows] usando git bash

```
git clone https://gitlab.com/linuxitos/dashtool.git
```

## Renombrar y mover proyecto

Renombrar el proyecto clonado a `dashtool` y moverlo a la ruta de instalación de xampp dentro del directorio htdocs:

Ruta de xampp por defecto en:

## [Windows]


```
C:\xampp\htdocs
```

## [Mac]

```
/Applications/XAMPP/htdocs
```


## [Linux]

```
/opt/lampp/htdocs
```


Mover la carpeta directa, a dicha ruta, si se requiere usar la terminal, se puede realizar con éste comando:

```
sudo mv /opt/lampp/htdocs
```

> En distribuciones linux, es necesario asignarle los siguientes permisos a la carpeta de dashtool, los siguientes permisos:

```
sudo chmod 775 -R /opt/lampp/htdocs/dashtool
```

```
sudo chown -R daemon:tu-usuario /opt/lampp/htdocs/dashtool
```

Reemplazar `tu-usuario` por el nombre de usuario que usen en su sistema operativo.

## Ejecutar composer install o update

Ejecutar el siguiente comando para instalar las dependencias del proyecto, para todos los sistemas operativos, aplica el mismo comando:

```
composer install
```

En windows: xampp por defecto viene con las extensiones zip y gd deshabilitadas, por lo que es necesario habilitarlas, para ésto:

Abrir el archivo php.ini de la ruta `/opt/lampp/etc/` y buscar las líneas que digan gd y zip, descomentar las líneas y reiniciar los servicios de xampp.


## Crear base de datos

Abrir en el navegador la ruta : {http://localhost/phpmyadmin}[http://localhost/phpmyadmin] y crear una base de datos con el nombre de ``dashtool`` y con tipo de cotejamiento utf8_spanish_ci

## Configurar base de datos

- Cambiar el nombre del archivo env => `.env` el archivo viene en la raíz del proyecto del directorio `dashtool`

- Configurar los datos de la base de datos en el archivo .env

```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=dashtool
DB_USERNAME=root
DB_PASSWORD=
```

- Configurar la url del proyecto de acuerdo a la ruta donde lo tengan almacenado, por defecto en xampp htdocs/

Abrir el archivo .env y buscar la línea:

```
APP_URL=http://localhost/dashtool/
```

## Ejecutar migraciones de tablas

Las tablas se crearán directo desde los comandos de laravel:

Para ésto, entrar al directorio del proyecto, y ejecutar los siguientes comandos:

```
php artisan migrate:fresh --seed
```

## Iniciar el proyecto

Para iniciar el proyecto no requiere php artisan serve, solo basta con abrir el navegador y abrir la url:

<http://localhost/dashtool>

- Por defecto se crea el siguiente usuario y contraseña:

```
andy@dev.com
holamundo
```

## Capturas

![alt tag](a.png)

![alt tag](b.png)

![alt tag](c.png)

![alt tag](d.png)

## MIT License

License: MIT

[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)

#### Developed By
----------------
 * linuxitos - <contact@linuxitos.com>