<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('folders', function (Blueprint $table) {
            $table->id();
            $table->tinyInteger('status')->default(1)->COMMENT('0 deleted, 1 active & private, 2 active & public, 3 active & pending');
            $table->timestamp('fc')->useCurrent();
            $table->string('desc', 1000)->nullable()->comment('Description file');
            $table->string('gname', 500)->nullable()->comment('Name file generate');
            $table->string('type', 255)->nullable()->comment('file type');
            $table->string('ext', 30)->default('png')->comment('extension file type');
            $table->integer('size')->default(0)->comment('file size in bytes');
            $table->string('path', 300)->nullable()->comment('path directory where saved files');
            $table->string('link', 512)->nullable()->comment('external link file');
            $table->foreignId('folder_id')->default(0)->comment('id folder parent');
            $table->foreignId('user_id')->references('id')->on('users')->constrained()->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('folders');
    }
};
